<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "habitacion".
 *
 * @property int $numhabit
 * @property int $idtipo
 *
 * @property Tipo $tipo
 * @property Reservas[] $reservas
 */
class Habitacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'habitacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idtipo'], 'integer'],
            [['idtipo'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['idtipo' => 'idtipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numhabit' => 'Numhabit',
            'idtipo' => 'Idtipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tipo::className(), ['idtipo' => 'idtipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reservas::className(), ['numhabit' => 'numhabit']);
    }
}
