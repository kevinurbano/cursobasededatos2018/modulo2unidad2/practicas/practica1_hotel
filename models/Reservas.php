<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reservas".
 *
 * @property int $codReserva
 * @property string $fechaEntrada
 * @property string $fechaSalida
 * @property int $numhabit
 * @property string $dni
 * @property double $iva
 *
 * @property Gastos $gastos
 * @property Clientes $dni0
 * @property Habitacion $numhabit0
 */
class Reservas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reservas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaEntrada', 'fechaSalida'], 'safe'],
            [['numhabit'], 'integer'],
            [['iva'], 'number'],
            [['dni'], 'string', 'max' => 20],
            [['dni'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['dni' => 'dni']],
            [['numhabit'], 'exist', 'skipOnError' => true, 'targetClass' => Habitacion::className(), 'targetAttribute' => ['numhabit' => 'numhabit']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codReserva' => 'Cod Reserva',
            'fechaEntrada' => 'Fecha Entrada',
            'fechaSalida' => 'Fecha Salida',
            'numhabit' => 'Numhabit',
            'dni' => 'Dni',
            'iva' => 'Iva',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasOne(Gastos::className(), ['codReserva' => 'codReserva']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDni0()
    {
        return $this->hasOne(Clientes::className(), ['dni' => 'dni']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNumhabit0()
    {
        return $this->hasOne(Habitacion::className(), ['numhabit' => 'numhabit']);
    }
}
