<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property string $dni
 * @property string $nombre
 * @property string $apellido
 * @property string $telefono
 * @property string $email
 * @property string $direccion
 * @property string $fechaNacimiento
 * @property string $poblacion
 * @property string $codPostal
 * @property string $provincia
 * @property string $pais
 *
 * @property Reservas[] $reservas
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['fechaNacimiento'], 'safe'],
            [['dni'], 'string', 'max' => 20],
            [['nombre', 'apellido'], 'string', 'max' => 40],
            [['telefono', 'poblacion', 'codPostal', 'provincia', 'pais'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 50],
            [['direccion'], 'string', 'max' => 60],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'direccion' => 'Direccion',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'poblacion' => 'Poblacion',
            'codPostal' => 'Cod Postal',
            'provincia' => 'Provincia',
            'pais' => 'Pais',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReservas()
    {
        return $this->hasMany(Reservas::className(), ['dni' => 'dni']);
    }
}
