<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gastos".
 *
 * @property int $idGastos
 * @property int $idGasto
 * @property int $codReserva
 * @property string $descGasto
 * @property double $importGasto
 *
 * @property Reservas $codReserva0
 */
class Gastos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gastos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idGasto', 'codReserva'], 'integer'],
            [['importGasto'], 'number'],
            [['descGasto'], 'string', 'max' => 50],
            [['idGasto'], 'unique'],
            [['codReserva'], 'unique'],
            [['codReserva'], 'exist', 'skipOnError' => true, 'targetClass' => Reservas::className(), 'targetAttribute' => ['codReserva' => 'codReserva']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idGastos' => 'Id Gastos',
            'idGasto' => 'Id Gasto',
            'codReserva' => 'Cod Reserva',
            'descGasto' => 'Desc Gasto',
            'importGasto' => 'Import Gasto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodReserva0()
    {
        return $this->hasOne(Reservas::className(), ['codReserva' => 'codReserva']);
    }
}
