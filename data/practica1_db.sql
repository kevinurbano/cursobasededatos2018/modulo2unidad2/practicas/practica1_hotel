﻿--  creacon de bd 
DROP DATABASE IF EXISTS practica1_hotel;
CREATE DATABASE practica1_hotel;
USE practica1_hotel;


--  tablas

CREATE TABLE tipo (
  idtipo int AUTO_INCREMENT PRIMARY KEY,
  categoria varchar(50),
  desripcion varchar(50),
  precioHabitacion float
);


CREATE TABLE habitacion (
  numhabit int AUTO_INCREMENT PRIMARY KEY,
  idtipo int
);


CREATE TABLE reservas (
  codReserva int AUTO_INCREMENT PRIMARY KEY,
  fechaEntrada date,
  fechaSalida date,
  numhabit int,
  dni varchar(20),
  iva float
);


CREATE TABLE clientes (
  dni varchar(20) PRIMARY KEY,
  nombre varchar(40),
  apellido varchar(40),
  telefono varchar(30),
  email varchar(50),
  direccion varchar(60),
  fechaNacimiento date,
  poblacion varchar(30),
  codPostal varchar(30),
  provincia varchar(30),
  pais varchar(30)
);


CREATE TABLE gastos (
  idGastos int AUTO_INCREMENT PRIMARY KEY,
  idGasto int,
  codReserva int,
  descGasto varchar(50),
  importGasto float
);

--  claves foraneas

  ALTER TABLE habitacion ADD CONSTRAINT  Fkhabitacion_tipo FOREIGN KEY  (idtipo) REFERENCES tipo(idtipo) ON DELETE CASCADE ON UPDATE CASCADE;

    ALTER TABLE reservas ADD CONSTRAINT  Fkreservas_habitacion FOREIGN KEY  (numhabit) REFERENCES habitacion(numhabit) ON DELETE CASCADE ON UPDATE CASCADE;

    ALTER TABLE reservas ADD CONSTRAINT  Fkreservas_cliente FOREIGN KEY  (dni) REFERENCES clientes(dni) ON DELETE CASCADE ON UPDATE CASCADE;

     ALTER TABLE gastos ADD CONSTRAINT  Fkgastos_rservas FOREIGN KEY  (codReserva) REFERENCES reservas(codReserva) ON DELETE CASCADE ON UPDATE CASCADE;


    --  unique 


      ALTER TABLE gastos ADD CONSTRAINT unique_gastos UNIQUE KEY (idGasto);

      ALTER TABLE gastos ADD CONSTRAINT unique_reserva UNIQUE KEY (codReserva);



